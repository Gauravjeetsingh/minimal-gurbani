let currentVerse = 0;
let currentAng = 0;
const baniDB = "https://api.banidb.com/v2/angs/";
const source = "G";
let fetchedData = [];
let isRemoteControlled = false;
let remoteId = "";

const fetchAng = (ang, verse = false) => {
  fetch(`${baniDB}${ang}/${source}`)
    .then((response) => response.json())
    .then((data) => {
      fetchedData = fetchedData.concat(data.page);
      if (verse) {
        currentVerse = verse;
      }
      showPankti();
    });
};

const showPankti = () => {
  const verseObj = fetchedData.filter(
    (pankti) => pankti.verseId == currentVerse
  )[0];

  const maxIndex = fetchedData.length - 1;
  const currentIndex = fetchedData.indexOf(verseObj);

  if (maxIndex - currentIndex < 5) {
    const isAngLoaded = currentAng == verseObj.pageNo + 1;
    if (!isAngLoaded) {
      currentAng = currentAng + 1;
      fetchAng(currentAng);
    }
  }

  localStorage.setItem("current-gurbani-ang", verseObj.pageNo);
  document.querySelector("#ang").textContent = `Ang ${verseObj.pageNo}`;
  document.querySelector("#pankti").textContent = verseObj.verse.unicode;
  document.querySelector("#translation").textContent =
    verseObj.translation.en.bdb;
};

const start = () => {
  document.querySelector("#startBtn").style.display = "none";
  document.querySelector(".gurbani").style.display = "block";
  const savedAng = localStorage.getItem("current-gurbani-ang");
  if (savedAng) {
    currentAng = parseInt(savedAng);
  } else {
    localStorage.setItem("current-gurbani-ang", 1);
    currentAng = 1;
  }
  const savedVerse = localStorage.getItem("current-gurbani-verse");
  if (savedVerse) {
    currentVerse = savedVerse;
  } else {
    localStorage.setItem("current-gurbani-verse", 1);
    currentVerse = 1;
  }
  fetchAng(currentAng);
};

const changePankti = () => {
  if (isRemoteControlled) {
    connectionDb.set({
      ang: parseInt(localStorage.getItem("current-gurbani-ang")),
      verse: currentVerse
    });
  }
  localStorage.setItem("current-gurbani-verse", currentVerse);
  showPankti();
};

const nextPankti = () => {
  currentVerse = parseInt(currentVerse) + 1;
  changePankti();
};

const prevPankti = () => {
  currentVerse = parseInt(currentVerse) - 1;
  changePankti();
};

document.addEventListener("keyup", (event) => {
  const keyName = event.key;

  if (keyName === "ArrowRight") {
    nextPankti();
  }

  if (keyName === "ArrowLeft") {
    prevPankti();
  }
});

const addRemote = (inputCode = "") => {
  const code = prompt("Enter the unique code to remote", inputCode);
  if (code) {
    if (code.length < 6) {
      alert(`The code is a bit short, add ${6 - code.length} more characters`);
      addRemote(code);
    } else {
      remoteId = code;
      isRemoteControlled = true;
      initializeRemote(remoteId);
      document.querySelector("#icon").style.opacity = 1;
    }
  }
};

document.querySelector(".gurbani").onclick = () => {
  nextPankti();
};

const updatePankti = (snapshot) => {
  var data = snapshot.val();
  if (data && data.verse && data.ang) {
    if (currentAng != data.ang) {
      fetchedData = [];
      fetchAng(data.ang, data.verse);
    } else {
      currentVerse = data.verse;
      changePankti();
    }
  }
};
